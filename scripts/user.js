var _       = require('underscore'),
    channel = require('./channel'),
    Handler = require('./handler').Handler;

module.exports = function(irc) {
	irc._user = {};

	irc.use(channel);

	irc.user = function(user) {
		if (typeof user != 'string')
			return this._user;

		return this._user[user] || (this._user[user] = new this.User({ mask: user }));
	};

	irc.User = User.bind(User, irc);
	_.extend(irc.User, User);
};


function User(client, data) {
	this._channel = {};
	this.client = client;
	this.mask(data.mask || '');
	this.mode(data.mode);
}

User.prototype = new Handler();
User.prototype.constructor = User;

User.prototype.mask = function(mask) {
	var match = mask.match(/^([^!@]+)(![^@]+)?(@.+)/);

	this.set('mask', mask);
	this.set('nick', match[1]);
	this.set('ident', match[2].substr(1));
	this.set('host', match[3].substr(1));
};

User.prototype.mode = function(channel, modes) {
	if (typeof modes == 'undefined') {
		modes = channel;
		channel = undefined;
	}
};

User.prototype.channel = function(channel) {
	if (typeof this._channel[channel] == 'undefined') {
		var self = this;
		this._channel[channel] = _.object(_.functions(this).map(function(method) {
			return [method, self[method].bind(self)];
		}));
	}

	return this._channel[channel];
};

function ChannelUser(client, data) {
}


exports.User = User;
