var _       = require('underscore'),
    user    = require('./user'),
    Handler = require('./handler').Handler;

module.exports = function(irc) {
	irc._channel = {};

	console.log(user);

	irc.use(user);

	irc.set('prefix', { 'o': '@', 'v': '+' });

	irc.on('disconnect', function() {
		_.values(irc._channel, function(channel) {
			channel.set('joined', false);
		});
	});

	irc.on('.005', function(event) {
		event.param.slice(1, -1).forEach(function(param) {
			param = param.split('=');

			switch (param[0]) {
				case 'CHANMODES':
					var modes = {};
					param[1].match(/(.,|.)/g).forEach(function(mode) {
						modes[mode[0]] = mode.length == 2;
					});

					irc.set('chanmodes', modes);
					break;

				case 'CHANTYPES':
					irc.set('chantypes', param[1].split(''));
					break;

				case 'PREFIX':
					irc.set('prefix', _.object(param[1].match(/^\([^)]+/)[0].substr(1).split(''), param[1].match(/[^)]+$/)[0].split('')));
					break;

				case 'UHNAMES':
					irc.set('uhnames', true);
					irc.sendUnbuf('PROTOCTL', 'UHNAMES');
					break;

				case 'NAMESX':
					irc.set('namesx', true);
					irc.sendUnbuf('PROTOCTL', 'NAMESX');
					break;
			}
		});
	})

	.on('.join', function(event) {
		var channel = irc.channel(event.param[0]),
		    user    = irc.user(event.prefix);

		if (user.get('nick') == irc.get('nick'))
			channel.set('joined', true);

		channel._user[user.get('nick')] = user.channel(channel.get('name'));

		channel.emit('join', {
			time: event.time,
			user: user
		});
	})

	.on('.part', function(event) {
		var channel = irc.channel(event.param[0]),
		    user    = irc.user(event.prefix);

		if (user.get('nick') == irc.get('nick'))
			channel.set('joined', false);

		delete channel._user[user.get('nick')];
		delete user._channel[channel.get('name')];

		channel.emit('part', {
			time: event.time,
			user: user
		});
	})

	.on('.quit', function(event) {
		console.log('quit', event);
	})

	.on('.topic .332', function(event) {
		var param   = event.param.slice(-2),
		    channel = irc.channel(param[0]);

		channel.emit('topic', {
			time: event.time,
			old: channel.get('topic'),
			topic: channel.set('topic', param[1]).get('topic')
		});
	})

	// name reply
	.on('.353', function(event) {
		var channel = irc.channel(event.param[2]);

		if (typeof channel.__replName == 'undefined')
			channel.__replName = [];

		channel.__replName = channel.__replName.concat(event.param.slice(-1)[0].split(' ').map(function(nick) {
			var modes = _.invert(irc.get('prefix'));
			nick = nick.match(new RegExp('^([\\' + _.keys(modes).join('\\') + ']*)([^!]+)'), '');

			return [nick[2], modes];
		}));
	})

	// name reply end
	.on('.366', function(event) {
		var channel = irc.channel(event.param[1]);
		console.log(channel.__replName);
		channel._user = _.object(channel.__replName);
		delete channel.__replName;

		if (!irc.get('namesx') || !irc.get('uhnames'))
			irc.send('who', channel.get('name'));
	})

	.on('.mode', function(event) {
		if (event.param[0][0] != '#') return;

		irc.channel(event.param[0]).emit('mode', {
			time: event.time
		});
	})

	.on('.privmsg', function(event) {
		if (event.param[0][0] != '#') return;

		irc.channel(event.param[0]).emit('message', {
			time: event.time,
			private: false,
			user: irc.user(event.prefix),
			message: event.param.slice(-1).pop()
		});
	});

	irc.channel = function(channel) {
		if (typeof channel != 'string')
			return this._channel;

		if (channel[0] != '#')
			channel = '#' + channel;

		return this._channel[channel] || (this._channel[channel] = new this.Channel(channel));
	};

	irc.join = function(channel, password) {
		this.send('join', channel);
		return this;
	};

	irc.part = function(channel) {
		this.channel(channel).part();
		return this;
	};

	irc.Channel = Channel.bind(Channel, irc);
};

function Channel(client, channel) {
	this._user  = {};
	this._mode  = {};
	this.client = client;
	this.set('name', channel);
	this.set('joined', false);
	this.set('topic', '');
}

Channel.prototype = new Handler();
Channel.prototype.constructor = Channel;


Channel.prototype.emit = function(event, data) {
	var ret = Handler.prototype.emit.call(this, event, data);

	this.client.emit(event, _.extend(data, {
		channel: this
	}));

	return ret;
};

Channel.prototype.topic = function(topic) {
	if (typeof topic == 'undefined' || topic === null)
		return this.get('topic');

	this.client.send('topic', this.get('name'), topic);
	return this;
};

Channel.prototype.mode = function(mode) {
	this.client.send.apply(this.client, ['mode', this.get('name')].concat(_.toArray(arguments)));
	return this;
};

Channel.prototype.part = function(message) {
	this.client.send('part', this.get('name'), message);
	return this;
};

Channel.prototype.join = function() {
	this.client.join(this.get('name'));
	return this;
};

Channel.prototype.cycle = function() {
	return this.part().join();
};

Channel.prototype.print = function(text, more) {
	this.client.print(text, _.extend(more || {}, {
		channel: this,
		text: text
	}));

	return this;
};

Channel.prototype.user = function(user) {
	this.client.user(user).channel(this.get('name'));
};

exports.Channel = Channel;
