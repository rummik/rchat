var $    = require('jquery'),
    _    = require('underscore'),
    swig = require('./swig');

var render = (function() {
	var templates = {};
	$('script[type="text/x-swig-template"]').each(function() {
		var $this = $(this);
		templates[$this.prop('id')] = swig.compile($this.html(), { filename: $this.prop('id') });
	});

	return function(template, data) {
		return templates[template](data || {});
	};
})();

$.fn.serializeObject = function() {
	var obj = {};

	this.find('input, checkbox, textarea, select').each(function() {
		var $this = $(this),
		    name  = $(this).prop('name');

		if (!name) return;

		obj[name] = $this.is('input[type=checkbox]')
		          ? $this.prop('checked')
		          : $this.val();
	});

	return obj;
};

$.fn.unserializeObject = function(obj) {
	var self = this;
	Object.keys(obj).forEach(function(name) {
		var $el = self.find('[name=' + name + ']');

		if ($el.is('input[type=checkbox]'))
			$el.prop('checked', obj[name]);
		else
			$el.val(obj[name]);

		$el.trigger('change');
	});
};

function WindowList(irc) {
	this.client = irc;

	irc.windows = this;
	irc.window  = new irc.Window({
		type: 'server',
		name: irc.get('network.name')
	});
}

WindowList.prototype = [];
WindowList.prototype.constructor = WindowList;

WindowList.prototype.find = function(name) {
	return this.filter(function(window) {
		return window.name == name;
	}).pop();
};

function Window(irc, opt) {
	this.client = irc;

	opt = opt || {};
	opt.name = opt.name || '';
	opt.type = opt.type || 'query';

	this.name = opt.name;

	if (opt.type != 'server')
		this.client.windows.push(this);

	var options = {
		type: opt.type,
		window_name: opt.name,
		window: opt.name.match(/[a-z0-9]+/ig).join('').toLowerCase(),
		network_name: irc.get('network.name'),
		network: irc.get('network')
	};

	$(render(options.type + 'Window', options)).appendTo('.windows');

	(options.type == 'server' ?
		$(render('windowlistNetwork', options)).appendTo('.windowlist') :
		$(render('windowlistTab', options)).appendTo('#network-' + options.network + ' ul')
	)
	.find('li a').trigger('click');
}

module.exports = function(irc) {
	$('.windowlist').on('click', 'li a', function(event) {
		var $this  = $(this);

		event.preventDefault();

		if ($this.hasClass('active'))
			return;

		$('.windowlist li.active, .windows div.active').removeClass('active');
		$this.parent().add($this.prop('href').match(/#.+$/)[0]).addClass('active');
	});

	irc.Window = Window.bind(Window, irc);
	irc.WindowList = WindowList.bind(WindowList, irc);
	irc.windows = new irc.WindowList();

	irc.on('.data', function(event) {
		if (/^\d+$/.test(event.command))
			this.print(event.param.slice(1).join(' '));
	})

	.on('print', function(event) {
		var scroll   = false,
		    $window  = $('#window-rizon-server table'),
		    $body    = $window.children('tbody'),
		    $message = $('<tr/>').append(
			    $('<td class="timestamp"/>').text(('' + new Date(event.time)).match(/\d+:\d+/)[0]),
			    $('<td/>').text(event.text)
		    );

		if ($window.scrollTop() >= $body.height() - $window.height())
			scroll = true;

		$body.append($message);

		if (scroll)
			$window.scrollTop($body.height() - $window.height());
	})

	.on('join', function(event) {
		var channel = event.channel,
		    window  = this.windows.find(channel.get('name'));

		if (!window) {
			window = new this.Window({
				type: 'channel',
				name: channel.get('name')
			});
		}

		window.channel = channel;
		channel.window = window;
	});

	// #settings modal
	$(function() {
		// load saved settings
		function loadSettings() {
			var settings = JSON.parse(localStorage.ircConfig || '{}');

			$('#settings form').unserializeObject(settings);

			Object.keys(settings).forEach(function(name) {
				irc.set(name, settings[name]);
			});
		}

		// save settings trigger
		function saveSettings() {
			var settings = $('#settings form').serializeObject();

			localStorage.ircConfig = JSON.stringify(settings);

			Object.keys(settings).forEach(function(name) {
				irc.set(name, settings[name]);
			});
		}

		// initialize settings (populate localStorage.ircConfig)
		loadSettings();
		saveSettings();

		// bind save/load to modal
		$('#settings').on('show', loadSettings);
		$('#settings-save').click(saveSettings);

		// fancy secondary nick thingy
		$('#settings form input[name=nick-primary]').on('keyup change', function() {
			$(this).siblings('[name=nick-secondary]').prop('placeholder', this.value + '-');
		});
	});

	// #connect modal
	$(function() {
		irc.set('ui.servers', irc.get('ui.servers') || [
			'localhost',
			'irc.rizon.net'
		]);

		var firstConnect = true,
		    $connect     = $('#connect');

		$connect.on('hide', function() {
			return !firstConnect;
		});

		irc.get('ui.servers').forEach(function(server) {
			$('#connect form select[name=servers]').append($('<option/>').text(server));
		});

		$('#connect-start').click(function() {
			var opts = $('#connect form').serializeObject();

			irc.connect({
				host: opts.server
			});

			opts.channels.split(/\s+/g).forEach(function(channel) {
				irc.join.apply(irc, channel.split(','));
			});

			firstConnect = false;
		});

		$('#connect').modal('show');
	});

	// typeahead plugin
	$(function() {
		// irc option defaults
		irc.set('ui.typeahead', irc.get('ui.typeahead') || true);
		irc.set('ui.typeahead.minlength', irc.get('ui.typeahead.minlength') || 3);

		var commands  = ['nick', 'help', 'about', 'join', 'part', 'query', 'msg', 'knock'].sort().map(function(cmd) { return '/' + cmd; }),
		    nicks     = ['Duis', 'Fusce', 'Aliquam', 'Nunc'].sort(),
		    items     = commands.concat(nicks),
		    block     = false,
		    index     = 0,
		    $shadow   = $('<div style="position:absolute;top:-200px;white-space:pre"/>').appendTo('body');
		    $complete = $('<ul class="dropdown-menu typeahead"><li>fa</li></ul>').appendTo('body'),
		    $command  = $('#command');

		$complete.on('mousedown', function(event) {
			block = true;
			$command.one('blur', function(event) {
				block = false;
				$(this).focus();
			});
		})

		.on('hover', 'a', function() {
			$complete.find('li').removeClass('active');
			index = $(this).parent().addClass('active').index();
		})

		.on('click', 'a', function(event) {
			var caret = $command.caret(),
			    text  = $(this).text(),
			    query = $command.val(),
			    start = query.substr(0, caret.start).split(' ').slice(0, -1).join(' '),
			    end   = query.substr(caret.start, query.length - caret.start).split(' ').slice(1).join(' ');

			start = (start.length ? start + ' ' : '') + text;

			$command.val(start + ' ' + end).caret(start.length + !end.length, start.length + !end.length);
			$complete.css('display', 'none');
			event.preventDefault();
		});

		$command.on('blur', function(event) {
			if (block) return;

			$complete.css('display', 'none');
		})

		.on('keydown', function(event) {
			// up: 40, down: 38, enter: 13
			if (event.which == 40 || event.which == 38 || event.which == 13) {
				if ($complete.css('display') == 'none')
					return;

				if (event.which != 13)
					index += event.which == 38 ? -1 : 1;
				else
					$complete.find('a').eq(index).click().parents('ul').css('display', 'none');

				var max = $complete.children().length - 1;
				index = index > max ? 0 : (index < 0 ? max : index);

				$complete.find('li').removeClass('active').eq(index).addClass('active');

				return false;
			}

			index = 0;

			_.defer(function() {
				var caret   = $command.caret(),
				    query   = $command.val().substr(0, caret.start),
				    lcQuery = query.toLowerCase(),
				    token   = query.split(' ').pop().toLowerCase(),
				    matches = [];

				if (!irc.get('ui.typeahead') || token.replace(/^\//, '').length < irc.get('ui.typeahead.minlength'))
					return $complete.css('display', 'none');

				// build list of matches
				items.forEach(function(item) {
					if (!token.length
					  ||(lcQuery != token && item[0] == '/')
					  ||(lcQuery == token && item.toLowerCase().indexOf(lcQuery) != 0)
					  ||(item.toLowerCase().indexOf(token) == -1))
						return;

					matches.push(item);
				});

				if (!matches.length)
					return $complete.css('display', 'none');

				$complete.empty();

				matches.forEach(function(match) {
					$complete.append($('<li/>').append($('<a href="#"/>').text(match)));
				});

				$complete.children('li').first().addClass('active');

				$complete.css({
					display: 'block',
					left: $command.offset().left + $shadow.text(query.split(/( )/g).slice(0, -1).join('')).width() + 8
				});
			});
		});
	});

	$(function() {
		$('.nav-tabs a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		})
	});
};

module.exports.Window = Window;
module.exports.WindowList = WindowList;