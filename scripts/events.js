var _ = require('underscore');

module.exports = function(irc) {
	irc.on('.ping', function(event) {
		this.sendUnbuf('pong', event.param);
	});

	irc.on('.nick', function(event) {
		//this.nick = event.param[0];
	});

	irc.on('.005', function(event) {
		event.param.slice(1, -1).forEach(function(param) {
			param = param.split('=');

			switch (param[0]) {
			}
		});
	});

	!function() {
		var motd = '';

		irc.on('.375', function() {
			motd = '';
		})

		.on('.372', function(event) {
			motd += (motd.length ? '\n' : '') + event.param.slice(-1);
		})

		.on('.376', function() {
			irc.motd = motd;
		});
	}();
};
