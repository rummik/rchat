module.exports = function(irc) {
	irc.on('.privmsg .notice', function(message) {
		if (!/^\001.*\001$/.test(message.param[message.param.length - 1]))
			return;

		var trailing = message.param[message.param.length - 1].replace(/^\001|\001$/g, '').split(' '),
		    ctcp = {
			    command: trailing[0],
			    param: trailing.slice(1),
			    target: message.prefix.split(/^([^!@]+)/g).slice(1, 2).shift(),
			    mask: message.prefix
		    };

		switch (message.command) {
			case 'PRIVMSG': irc.emit('ctcp', ctcp); break;
			case 'NOTICE':  irc.emit('nctcp', ctcp); break;
			default: return;
		}

		return false;
	})

	.on('ctcp', function(ctcp) {
		switch (ctcp.command) {
			case 'REFERRAL':
			case 'WEBSITE': irc.send('nctcp', [ctcp.target, ctcp.command, location.protocol + '//' + location.host + location.pathname]); break;
			case 'PING': irc.send('nctcp', [ctcp.target, 'PING', ctcp.param.join(' ')]); break;
		}
	})

	.bind('ctcp', function(params) {
		irc.send('privmsg', [params.shift(), ':\001' + params.shift().toUpperCase() + ' ' + params.join(' ') + '\001']);
	})

	.bind('nctcp', function(params) {
		irc.send('notice', [params.shift(), ':\001' + params.shift().toUpperCase() + ' ' + params.join(' ') + '\001']);
	});
};
