var _        = require('underscore'),
    io       = require('socket.io'),
    events   = require('./events'),
    defaults = require('./defaults'),
    channel  = require('./channel'),
    Handler  = require('./handler').Handler;

// todo: buffering

function IRC(network) {
	var self = this;
	this._commands   = {};
	this._use        = [];
	this._queue      = {
		buffer: [],
		ticks: 0,
		throttled: false,
		interval: undefined,

		push: function(send) {
			this.buffer.push(send);

			if (++this.ticks > 3)
				this.throttled = true;
			else
				this.shift();

			if (typeof this.interval == 'undefined')
				this.interval = setInterval(this.tick.bind(this), 1000);
		},

		shift: function(count) {
			for (var i=0; i<(count || 1) && this.buffer.length && self.status == 'connected'; ++i)
				self.sendUnbuf.apply(self, this.buffer.shift());
		},

		tick: function() {
			this.shift();

			if (this.ticks > 0 && --this.ticks == 0) {
				this.throttled = false;
				clearInterval(this.interval);
				this.interval = undefined;
			}
		}
	};

	this.status = 'init';

	this.use(defaults);
	this.use(events);

	this.set('network', network.match(/[a-z0-9]+/ig).join('').toLowerCase());
	this.set('network.name', network);

	if (process.title == 'browser') {
		this._socket = io.connect(location.protocol + '//' + location.host, {
			'force new connection': true
		});

		// on:   socket.connect, socket.data, socket.close
		// emit: socket.open, socket.write, socket.closed
		this._socket.on('socket.connect', this.emit.bind(this, '.socket.connect'));
		this._socket.on('socket.closed', this.emit.bind(this, '.socket.closed'));

		this._socket.on('socket.data', this.parse.bind(this));

		this._socket.on('disconnect', function() {
			self.status = 'disconnected';
		});

		this._socket.on('reconnect', function() {
			self.reconnect();
		});
	}

	this.on('.001 .002 .003 .004 .005', _.debounce(function() {
		self.status = 'connected';
		self._queue.shift(4);
	}, 100));
}

IRC.prototype = new Handler();
IRC.prototype.constructor = IRC;

/**
 * Message parser.  Also distributes events
 * @param {string} message Message from server to parse
 * @param {number} [time=Date.now()] Unix timestamp of when the message was received
 * @return {IRC}
 */
IRC.prototype.parse = function(message, time) {
	var self = this;

	time = time || Date.now();

	message.split(/\r\n/g).forEach(function(line) {
		if (!line.length) return;

		var match = line.match(/^((?::[^\0\r\n ]+ )?)([a-z]+|[0-9]{3})((?: [^\0\r\n :][^\0\r\n ]*){0,14})(( :?[^\0\r\n]*)?)/i),
		    middle = match[3].length ? match[3].substr(1).split(' ') : [],
		    message = {
			    time: time,
			    raw: line,
			    prefix: match[1].substr(1, match[1].length - 2),
			    command: match[2],
			    param: middle.concat(match[4].substr(2))
		    };

		self.emit('.data', message);

		if (!self.emit('.' + message.command.toLowerCase(), message))
			self.emit('.unbound', message);
	});

	return this;
};

/**
 * Connect to a server
 * @param {object} options
 * @return {IRC}
 */
IRC.prototype.connect = function(options) {
	this.status = 'connecting';
	options = options || {};

	this.set('host', options.host || this.get('host'));
	this.set('port', options.port || this.get('port'));

	this._socket.emit('socket.open', { host: this.get('host'), port: this.get('port') });

	this.set('nick', options.nick || this.get('nick'));
	this.set('name', options.name || this.get('name'));
	this.set('ident', options.ident || this.get('ident'));
	this.set('pass', options.pass || this.get('pass'));

	if (typeof this.get('pass') != 'undefined')
		this.sendUnbuf('pass', this.get('pass'));

	this.sendUnbuf('nick', this.get('nick'));
	this.sendUnbuf('user', [ this.get('ident'), 8, '*', this.get('name') ]);

	return this;
};

/**
 * Reconnect to server.  See .connect() for more info
 * @param {object} options
 * @return {IRC}
 */
IRC.prototype.reconnect = function() {
	return this.disconnect().connect.apply(this, arguments);
};

/**
 * Disconnect from server
 * @return {IRC}
 */
IRC.prototype.disconnect = function() {
	this.status = 'disconnected';
	this.emit('disconnect');

	return this;
};

/**
 * Emit a print event
 * @param {string} text
 * @param {object} [data] Optional data to pass along with the event
 * @return {IRC}
 */
IRC.prototype.print = function(text, data) {
	data = data || {};
	this.emit('print', _.extend(data, { time: data.time || Date.now(), text: text }));
	return this;
};

/**
 * Use a plugin
 * @param {function} plugin
 * @return {IRC}
 */
IRC.prototype.use = function(plugin) {
	var exit = false;

	this._use.forEach(function(w) {
		if (w === plugin)
			exit = true;
	});

	if (!exit && _.isFunction(plugin)) {
		plugin.call(this, this);
		this._use.push(plugin);
	}

	return this;
};


/**
 * Bind a command
 * @param {string} command Name of the command to be bound
 * @param {function} fn fn(data)
 * @return {IRC}
 */
IRC.prototype.bind = function(command, fn) {
	this._commands[command] = fn;
	return this;
};

/**
 * Sends a command to the server
 * IRC.send('knock', '#channel')
 * IRC.send('mode', '#channel', '+v', 'someone');
 * @param {string} command
 * @param {...string|array} params
 * @return {IRC}
 */
IRC.prototype.send = function() {
	// push the arguments on the queue stack to later hit .sendUnbuf()
	this._queue.push(arguments);
	return this;
};

/**
 * Takes the same arguments as IRC.send(), but sends without touching the message queue
 * @param {string} command
 * @param {...string|array} params
 * @return {IRC}
 */
IRC.prototype.sendUnbuf = function(command, params) {
	if (typeof params == 'undefined')
		params = [];

	if (typeof params == 'string')
		params = [ params ];

	if (arguments.length > 2)
		params = params.concat(Array.prototype.slice.call(arguments, 2));

	params = params.filter(function(v) { return typeof v != 'undefined'; });

	// todo: make this not potentially recursive
	if (this._commands.hasOwnProperty(command) &&
	    typeof this._commands[command] == 'function') {
		this._commands[command].call(this, params);
		return;
	}

	// automatically add : to trailing parameter if necessary
	var trailing = params[params.length - 1];
	if (trailing[0] != ':' && /[ :]/.test(trailing))
		params[params.length - 1] = ':' + trailing;

	this.raw(command.toUpperCase() + (params.length ? ' ' + params.join(' ') : ''));

	return this;
};

/**
 * Send a raw message to the server
 * IRC.raw('PRIVMSG #channel :Hello!')
 * @param {string} data
 * @return {IRC}
 */
IRC.prototype.raw = function(data) {
	console.log(data);
	this._socket.emit('socket.write', data + '\r\n');
	return this;
};


IRC.Channel    = channel.Channel;

IRC.channel    = channel;

module.exports = IRC;
