var _ = require('underscore');

function Handler() {}

/**
 * Emit events
 * @param {string} event Event name
 * @param {object} data Data to pass to event listeners
 * return {bool} False if no events are bound, true otherwise
 */
Handler.prototype.emit = function(event, data) {
	var self = this,
	    stop = false;

	data = _.clone(data);

	if (typeof this._events == 'undefined')
		this._events = {};

	if (typeof this._events[event] == 'undefined')
		return false;

	this._events[event].forEach(function(fn) {
		if (stop) return;

		if (fn.call(self, data) === false)
			stop = true;
	});

	return true;
};

/**
 * Bind listener to an event
 * @method {string} event Event(s) to bind
 * @method {function} fn Listener
 * @return this
 */
Handler.prototype.on = function(event, fn) {
	var self = this;

	if (typeof this._events == 'undefined')
		this._events = {};

	if (event.indexOf(' ') == -1) {
		if (!Array.isArray(this._events[event]))
			this._events[event] = [];

		this._events[event].push(fn);
	} else {
		event.split(/ +/g).forEach(function(event) {
			self.on(event, fn);
		});
	}

	return this;
};


/**
 * Set the value of an option
 * @param {...string} [namespace]
 * @param {string} option
 * @param {string} value
 * @return this
 */
Handler.prototype.set = function() {
	var args   = _(_.toArray(arguments)),
	    option = args.initial().join('.'),
	    value  = args.last();

	if (typeof this._set == 'undefined')
		this._set = {};

	if (typeof value !== 'undefined')
		this._set[option] = value;

	return this;
};

/**
 * Get the value of an option
 * @param {...string} [namespace]
 * @param {string} option
 * @return this
 */
Handler.prototype.get = function() {
	var option = _.toArray(arguments).join('.');

	if (typeof this._set == 'undefined')
		this._set = {};

	return this._set[option];
};


exports.Handler = Handler;
