Installation
============
1. Clone the repo
2. `npm install`
3. Edit config.json (check out config.json.sample)
4. `foreman start` -- or just run `node app.js`


Scripting
=========
Currently events are rather minimal, and messages are parsed into the following format:
```json
{
	"command": "COMMAND",
	"param": ["param1", "param2", "trailing parameter"],
	"prefix": "command_prefix",
	"raw": ":command_prefix COMMAND param1 param2 :trailing parameter"
}
```

IRC.on('event [event ...]', callback(message))
----------------------------------------------
Unfiltered events are prefixed with '.'
```js
irc.on('.ping', function(message) {
	this.send('pong', message.param);
});
```

IRC.connect(options)
--------------------
```js
irc.connect({
	host: 'localhost',
	port: 6667,             // optional, defaults to 6667
	pass: 'serverpassword', // optional

	nick: 'mynick',
	name: 'rChat User',     // optional, defaults to 'rChat User'
	ident: 'rchat'          // optional, defaults to 'rchat'
});
```

IRC.bind('command', function(params))
-------------------------------------
```js
irc.bind('ctcp', function(params) {
	irc.send('privmsg', [params.shift(), ':\001' + params.join(' ') + '\001']);
});
```

IRC.send('command', params)
---------------------------
```js
irc.send('nick', nick);
irc.send('user', [ ident, 8, '*', name ]);
```
