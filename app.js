var socks    = {},
    express     = require('express'),

    app         = express(),
    server      = require('http').createServer(app),
    io          = require('socket.io').listen(server),

    cons        = require('consolidate'),
    swig        = require('swig'),

    browserify  = require('browserify'),
    stylus      = require('stylus'),
    chromeframe = require('express-chromeframe'),
    nib         = require('nib'),

    net         = require('net'),
    fs          = require('fs'),

    config      = JSON.parse(fs.readFileSync(__dirname + '/config.json')),
    argv        = require('optimist')
			.options('port', {
				alias: 'p',
				default: config.port || 9090,
					 describe: 'incoming port'
			})
			.options('host', {
				alias: 'h',
				default: config.host || '',
					 describe: 'incoming host'
			})
			.argv;

app.engine('.html', cons.swig);
app.set('view engine', 'html');

swig.init({
	root: app.get('views'),
	allowErrors: true,
	cache: false
});

app.use(chromeframe());

app.use(
	browserify({
		watch: true,
		require: {
			jquery: 'jquery-browserify',
			'socket.io': 'socket.io-browserify'
		}
	})
	.addEntry(__dirname + '/scripts/index.js')
);

app.use(stylus.middleware({
	src:  __dirname + '/styles',
	dest: __dirname + '/public/assets',
	compile: function(str, path) {
		return stylus(str)
			.set('filename', path)
			.set('compress', false)
			//.set('linenos', true)
			.use(nib());
	}
}));

app.use(app.router);

app.use(express.static(__dirname + '/public/assets'));

server.listen(argv.port, argv.host);


app.get('/', function(req, res) {
	res.render('index', { brand: config.brand || 'rChat' });
});

io.configure(function () {
	io.set('transports', config.io_transports || [ 'xhr-polling' ]);
	io.set('polling duration', 10);
});

io.configure('production', function() {
	io.set('log level', 1);
});

io.sockets.on('connection', function (socket) {
	var sock;

	socket.on('socket.open', function(data) {
		if (!/^[a-z0-9._-]+$/.test(data.host))
			return;

		if (config.whitelist !== false) {
			var connect = false;
			data.port = +data.port;

			if (data.host in config.whitelist) {
				config.whitelist[data.host].forEach(function(port) {
					if (data.port === port)
						connect = true;
				});
			}

			if (!connect) {
				socket.emit('socket.error', 'host/port not in whitelist');
				return;
			}
		}

		socket.emit('socket.connect', { 'status': 'connecting' });

		sock = new net.Socket({
			type: data.useIPv6 ? 'tcp6' : 'tcp4',
			allowHalfOpen: true
		});

		sock.on('error', function(error) {
			socket.emit('socket.error', error);
		});

		sock.on('data', function(data) {
			socket.emit('socket.data', data.toString(), Date.now());
		});

		sock.on('close', function() {
			socket.emit('socket.closed');
			sock = undefined;
		});

		sock.connect(data.port, data.host, function() {
			socket.emit('socket.connect', { 'status': 'ready' });
		});
	});

	socket.on('socket.write', function(data) {
		if (typeof sock == 'undefined' || typeof data != 'string')
			return;

		if (!/^quit\W/i.test(data))
			sock.write(data);
		else
			sock.end('QUIT :' + config.quit_message || 'rChat - https://bitbucket.org/rummik/rchat/');
	});

	socket.on('socket.close', function() {
		if (typeof sock == 'undefined')
			return;

		sock.end('QUIT :' + config.quit_message || 'rChat - https://bitbucket.org/rummik/rchat/');
	});

	socket.on('disconnect', function() {
		if (typeof sock == 'undefined')
			return;

		sock.end('QUIT :' + config.quit_message || 'rChat - https://bitbucket.org/rummik/rchat/');
	});
});
